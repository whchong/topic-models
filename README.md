# NMF topic models

**Topic modeling with Non-negative matrix factorization**

Minimizes the following loss function using multiplicative updates:

<img src="https://render.githubusercontent.com/render/math?math=||X-WH||^{2}_{Fro} %2B \alpha(||W||_1%2B||H||_1)">

Let D be no. of documents and V be the vocab size. **X** is (D x V) data matrix. Each row is a document and each column is a feature, e.g. textual/visual word. **W** is document-topic matrix of dimension (D x K) where K is the number of topics. **H** is topic-word matrix of dimension (K x V). 

The function that does the NMF is called SMU_NMF. See toy_demo.py for example usage with fake toy data with dense **X**. For real data, **X** should be a sparse matrix, e.g. scipy.sparse.csr_matrix.

See topics.py for an example that loads a small set of text data ('text.txt'), forms sparse matrix **X** and infers the topics.
Top words for each topics are printed out.

**A variant**

Alternatively for the version where regularization for H and W are untied, we have: 

<img src="https://render.githubusercontent.com/render/math?math=||X-WH||^{2}_{Fro} %2B \alpha_1||H||_1 %2B \alpha_2||W||_1">

This is implemented by the function SMU_NMF_untied.

**Evaluating Coherence**

'dso_coherence.py' implements the coherence metrics **c_v** and **c_npmi**, as discussed in [1]. Note that **c_v** is indicated as the better performing metrics. We implement this as the metrics are wrongly computed in open source packages, e.g. Gensim, Tomotopy (contact Wen Haw for details). 

For usage, see 'topics.py' for an example that computes **c_v** for topics from NMF. If you will like to explore other coherence metrics, see 'gensim_coherence.py' which is a wrapper around the numerous published coherence metrics. The wrapper function takes in data structures of the corpus and topics, hence allowing metric computation without using the Gensim LDA model. See "coherence_demo.py" for a demo.

[1] 	Michael Röder, Andreas Both, Alexander Hinneburg, Exploring the Space of Topic Coherence Measures. WSDM 2015: 399-408

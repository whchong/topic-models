import tomotopy as tp
from dso_coherence import compute_coherence_readmodel, compute_coherence_readfile
from gensim_coherence import get_coherence

topn=30     #  top words in each topic to consider
preset='c_v'    # only support c_v, c_npmi
model=tp.LDAModel(k=10)
for line in open('text.txt'):
    model.add_doc(line.strip().split())
    
model.train(0)
print('before training')    
[ave_coh, topic_scores]=compute_coherence_readmodel(model, topn, preset)
print('dso score', ave_coh)    

coh = tp.coherence.Coherence(model, coherence=preset, top_n=topn)
tmtp_score = coh.get_score()
print('tmtp_score', tmtp_score)

if preset=='cv':
    gs_score=get_coherence(model, coherence=preset, topn=topn, window_size=110, processes=1)
else:    
    gs_score=get_coherence(model, coherence=preset, topn=topn, window_size=10, processes=1)
print('gensim-coherence', gs_score)


model.train(500)

[ave_coh, topic_scores]=compute_coherence_readmodel(model, topn, preset)
print('after training')
print('dso score', ave_coh)    

coh = tp.coherence.Coherence(model, coherence=preset, top_n=topn)
tmtp_score = coh.get_score()
print('tmtp_score', tmtp_score)

if preset=='cv':
    gs_score=get_coherence(model, coherence=preset, topn=topn, window_size=110, processes=1)
else:    
    gs_score=get_coherence(model, coherence=preset, topn=topn, window_size=10, processes=1)
print('gensim-coherence', gs_score)

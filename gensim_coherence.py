# calculating coherence using gensim's coherence module
# https://github.com/bab2min/tomotopy/issues/73
# easier to use than tomotopy's coherence module (invoked from their topic models)

import collections

import gensim

def get_coherence(
        model, coherence, topn, window_size, processes=None
    ):
    """
    Calculates the coherence score for a given Tomotopy model via Gensim's 
    `coherencemodel` pipeline.  
    
    Parameters
    ----------
    model: Tomotopy.LDAModel
        The Tomotopy model to get coherence scores for.
    
    coherence: str, optional
    topn: int, optional
    window_size: int, optional
    processes: int, optional
        All of these parameters are passed directly to 
        `gensim.models.coherencemodel.CoherenceModel`, and the Gensim defaults will 
        apply if they are omitted.
        
    Returns
    -------
    float
        The coherence score for the model.
    """

    topics = []
    for k in range(model.k):
        word_probs = model.get_topic_words(k, topn)
        topics.append([word for word, prob in word_probs])  # only append word. Probability not needed
    
    texts = []
    corpus = []
    for doc in model.docs:
        words = [model.vocabs[token_id] for token_id in doc.words]
        texts.append(words)
        freqs = list(collections.Counter(doc.words).items())    # each list element=(wid, wid_freq)
        corpus.append(freqs)
    
    id2word = dict(enumerate(model.vocabs))
    dictionary = gensim.corpora.dictionary.Dictionary.from_corpus(
        corpus, id2word
    )
    cm = gensim.models.coherencemodel.CoherenceModel(
        topics=topics,
        texts=texts,
        corpus=corpus,
        dictionary=dictionary,
        window_size=window_size,
        coherence=coherence,
        topn=topn,
        processes=processes,
    )
    chk=cm.get_coherence_per_topic() #topics)
    print('texts', len(texts))
    print('gensim per topic')
    for ii in range(0, model.k):
        print('gensim',ii,chk[ii])

    return cm.get_coherence()

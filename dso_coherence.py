import numpy as np

def build_index(documents, topics, win_size):
    word_docs=dict()
    for topn_words in topics:
        for w in topn_words:
            if w not in word_docs:
                word_docs[w]=set()
                   
    docid=0
    for doc in documents:
        if len(doc) <= win_size:    # no need slide
            for w in doc:
                if w in word_docs:
                    word_docs[w].add(docid)
            docid+=1
        else:
            left=0
            for right in range(win_size, len(doc)+1):
                win_doc=set(doc[left:right])
                for w in win_doc:
                    if w in word_docs:
                        word_docs[w].add(docid)
                left+=1
                docid+=1
    return [word_docs, docid]
    
def all_topics_coherence_Cnpmi(documents, topics):
    win_size=10
    topic_scores=[]
    [word_docs, doc_cnt]=build_index(documents, topics, win_size)   # index once to support all topics
    for topn_words in topics:   # topn words is a list
        n=len(topn_words)
        w_cnt=np.zeros(n)
        ww_cnt=np.zeros((n,n))
        
        # occurrence count
        for i in range(0, n):
            w=topn_words[i]
            w_cnt[i]=len(word_docs[w])      # count of doc that w occurs in, including sliding window
            
        # co-occurrence count via set intersection
        for i in range(0, n):
            wi=topn_words[i]
            for j in range(i+1, n):
                wj=topn_words[j]
                common_docs=word_docs[wi].intersection(word_docs[wj])
                ww_cnt[i,j]=len(common_docs)
                ww_cnt[j,i]=len(common_docs)
                
        np.fill_diagonal(ww_cnt, w_cnt)     # count(w_i,w_i)=count(w_i).    done in both gensim, tomotopy
        p_w = w_cnt / doc_cnt
        p_ww = ww_cnt / doc_cnt
        NPMI = compute_NPMI(p_ww, p_w)      # n * n, symmetric matrix with 1 on diagonal
        
        allpair_sum = np.sum(NPMI) - n  # exclude diagonal. one-one segmentation. Following the code logic in gensim, tomotopy
        num_pair = n * (n-1)
        score = allpair_sum / num_pair
        topic_scores.append(score)
    ave_score = np.mean(topic_scores)
    
    return [ave_score, topic_scores]
                

def all_topics_coherence_Cv(documents, topics):
    win_size=110
    topic_scores=[]
    [word_docs, doc_cnt]=build_index(documents, topics, win_size)   # index once to support all topics
    for topn_words in topics:   # topn words is a list
        n=len(topn_words)
        w_cnt=np.zeros(n)
        ww_cnt=np.zeros((n,n))
        
        # occurrence count
        for i in range(0, n):
            w=topn_words[i]
            w_cnt[i]=len(word_docs[w])      # count of doc that w occurs in, including sliding window
            
        # co-occurrence count via set intersection
        for i in range(0, n):
            wi=topn_words[i]
            for j in range(i+1, n):
                wj=topn_words[j]
                common_docs=word_docs[wi].intersection(word_docs[wj])
                ww_cnt[i,j]=len(common_docs)
                ww_cnt[j,i]=len(common_docs)
                
        np.fill_diagonal(ww_cnt, w_cnt)     # count(w_i,w_i)=count(w_i).    done in both gensim, tomotopy
        p_w = w_cnt / doc_cnt
        p_ww = ww_cnt / doc_cnt
        NPMI = compute_NPMI(p_ww, p_w)      # n * n, symmetric matrix with 1 on diagonal
        vec_W=np.sum(NPMI, axis=1)
        vec_W_normalized = vec_W / np.linalg.norm(vec_W)
        cossim_list=[]        
        for i in range(0, n):
            vec_w = NPMI[i,:]
            cos_sim = np.dot(vec_w, vec_W_normalized) / np.linalg.norm(vec_w)
            cossim_list.append(cos_sim)
        ave_cos_sim = np.mean(cossim_list)
        topic_scores.append(ave_cos_sim)
    ave_score = np.mean(topic_scores)

    return [ave_score, topic_scores]


def show_counts(topn_map, w_cnt, ww_cnt):
    n=len(topn_map)
    revmap=dict()
    print("========")
    for w, wid in topn_map.items():
        print(w, w_cnt[wid])
        revmap[wid]=w
    for i in range(0,n):
        wi=revmap[i]
        for j in range(i+1, n):
            wj=revmap[j]
            print(wi, wj, ww_cnt[i,j])
    print("========")

# for 1 topic
def topic_coherence(documents, topn_words):
    win_size=110
    n=len(topn_words)
    topn_map=dict()
    for i in range(0, n):
        w=topn_words[i]
        topn_map[w]=i

    w_cnt=np.zeros(n)
    ww_cnt=np.zeros((n,n))
    doc_cnt=0
    for doc in documents:
        if len(doc) <= win_size:    # sliding not possible
            update_counts(doc, w_cnt, ww_cnt, topn_map)
            doc_cnt+=1
        else:
            left=0
            for right in range(win_size, len(doc)+1):
                win_doc=doc[left:right]
                update_counts(win_doc, w_cnt, ww_cnt, topn_map)
                left+=1
                doc_cnt+=1
    np.fill_diagonal(ww_cnt, w_cnt)   # count(w_i, w_i)=count(w_i). gensim: diagonal should be equal to occurrence counts   
    # show_counts(topn_map, w_cnt, ww_cnt)    

    p_w = w_cnt / doc_cnt
    p_ww = ww_cnt / doc_cnt
    NPMI = compute_NPMI(p_ww, p_w)  # n*n symmetric matrix with 1 on diagonal
    vec_W=np.sum(NPMI, axis=1)
    vec_W_normalized = vec_W / np.linalg.norm(vec_W)
    ave_cos_sim=0.0
    for i in range(0, n):
        vec_w = NPMI[i,:]
        cos_sim = np.dot(vec_w, vec_W_normalized) / np.linalg.norm(vec_w)
        ave_cos_sim += cos_sim
    ave_cos_sim /= n
    return ave_cos_sim
    
# symmetric matrix
def compute_NPMI(p_ww, p_w):
    #eps=np.finfo(float).eps
    eps=np.power(10.0,-12)   # to be in sync with gensim, tomotopy
    log_pww=np.log(p_ww + eps)
    pw_pw=np.outer(p_w, p_w)
    PMI = np.log( np.divide( p_ww + eps, pw_pw) )   # in paper & gensim
    #PMI = np.log( np.divide( p_ww + eps, pw_pw + eps) ) # done in tomotopy
    NPMI = np.divide( PMI, -log_pww)
    return NPMI

# count of docs where word occurs in. Not word counts
def update_counts(doc, w_cnt, ww_cnt, topn_map):
    hits=set()
    for w in doc:
        if w in topn_map:
            wid=topn_map[w]            
            hits.add(wid)
            
    for wid in hits:
        w_cnt[wid]+=1
        
    if len(hits)>1:
        wlist=list(hits)
        for i in range(0, len(wlist)):
            wi=wlist[i]
            for j in range(i+1, len(wlist)):
                wj=wlist[j]
                ww_cnt[wi, wj]+=1
                ww_cnt[wj, wi]+=1


def count_co_occuurrence(w1, w2):
    joint=0
    ave_doc_len=0
    fp=open('text_filtered.txt','r')
    for line in fp:
        token=line.strip().split(' ')
        wordset=set(tokens)
        if (w1 in wordset) and (w2 in wordset):
            joint+=1
            ave_doc_len += len(tokens)
    fp.close()
    print(w1, w2, joint)
    ave_doc_len /= joint
    print('ave_doc_len', ave_doc_len)        
            
            
# model = tomotopy LDA model
def compute_coherence_readmodel(model, topn, preset):
    documents=[]
    used_vocab=set(model.used_vocabs)
    ii=0
    for doc in model.docs:
        words = [model.vocabs[token_id] for token_id in doc.words]
        doc=[]
        for w in words:
            if w in used_vocab:
                doc.append(w)
        if len(doc) > 0:
            documents.append(doc)
        ii+=1
    print('model documents', len(documents), len(model.docs))

    topics=[]
    for k in range(model.k):
        topn_info = model.get_topic_words(k, top_n=topn)
        topn_words=[]
        for w, prob in topn_info:
            topn_words.append(w)
        topics.append(topn_words)
    
    if preset=='c_npmi':
        print(preset)
        [ave_score, topic_scores]=all_topics_coherence_Cnpmi(documents, topics)
    else:
        print("c_v")
        [ave_score, topic_scores]=all_topics_coherence_Cv(documents, topics)
    #print('topn', topn, ' ave coherence', ave_score)
    return [ave_score, topic_scores]
                
                
def compute_coherence_readfile(textfile, topicfile, topn, preset):
    documents=[]
    fp=open(textfile, 'r')
    for line in fp:
        doc=line.strip().split(' ')
        documents.append(doc)
    fp.close()
    print('documents', len(documents))

    topics=[]
    fp=open(topicfile, 'r')
    for line in fp:
        topn_words=line.strip().split(' ')
        topics.append(topn_words[:topn])
    fp.close()
    
    if preset=='c_npmi':
        print(preset)
        [ave_score, topic_scores]=all_topics_coherence_Cnpmi(documents, topics)
    else:
        print("c_v")
        [ave_score, topic_scores]=all_topics_coherence_Cv(documents, topics)
    print('topn', topn, ' ave coherence', ave_score)
    return [ave_score, topic_scores]
    
# slow. Not recommended
def compute_coherence_naive(textfile, topicfile, topn):
    documents=[]
    fp=open(textfile, 'r')
    for line in fp:
        doc=line.strip().split(' ')
        documents.append(doc)
    fp.close()
    
    n=0
    ave_score=0.0
    fp=open(topicfile, 'r')
    for line in fp:
        topn_words=line.strip().split(' ')
        topn_words=topn_words[:topn]
        score=topic_coherence(documents, topn_words)
        ave_score+=score
        n+=1
    fp.close()
    ave_score/=n
    print('topn', topn, ' ave coherence', ave_score)
    return ave_score
    
    
    
    
                


